﻿using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using NodaTime;
using System.Collections.Generic;


namespace Service
{
    public class InfluxService
    {
        string Bucket;
        string Org;
        DateTimeZone systemZone = DateTimeZoneProviders.Tzdb.GetSystemDefault();
        InfluxDBClient client;
        List<KeyValuePair<string, string>> configValues;
        public InfluxService(string host, string bucket, string org, string token)
        {
            Org = org;
            Bucket = bucket;
            client = InfluxDBClientFactory.Create(host, token);
        }

        public void AlmacenarValor(string lugar, int ingresos, int salidas, int total)
        {
            using (var writeApi = client.GetWriteApi())
            {
                var punto = PointData.Measurement("Contador")
                    .Tag("Lugar", lugar)
                    .Field("Entrantes", ingresos)
                    .Field("Salientes", salidas)
                    .Field("Total", total)
                    .Timestamp(DateTime.UtcNow, InfluxDB.Client.Api.Domain.WritePrecision.Ms);

                writeApi.WritePoint(punto, Bucket, Org);
            }
        }


        public List<DatoAfluencia> ObtenerDatosDeLugar(string torniquete, string evento)
        {
            return ObtenerDatosDeTorniqueteAsync(torniquete, evento).Result;
        }

        private async Task<List<DatoAfluencia>> ObtenerDatosDeTorniqueteAsync(string lugar, string evento)
        {

            List<DatoAfluencia> datos = new List<DatoAfluencia>();
            string query = $"from(bucket: \"{Bucket}\") |> range(start: -1h) |> filter(fn: (r) => r[\"_measurement\"] == \"Contador\") |> filter(fn: (r) => r[\"Lugar\"] == \"{lugar}\") |> filter(fn: (r) => r[\"_field\"] == \"{evento}\")";

            var result = await client.GetQueryApi().QueryAsync(query, Org);
            result.ForEach(tabla =>
            {
                var data = tabla.Records;
                data.ForEach(data =>
                {
                    datos.Add(new DatoAfluencia(data.GetTimeInDateTime().Value, data.GetField(), (long)data.GetValue()));
                });
            });
            return datos;
        }
    }
}
