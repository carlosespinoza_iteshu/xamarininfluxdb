﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class DatoAfluencia
    {

        public DatoAfluencia(DateTime fechaHora, string lugar, long valor)
        {
            FechaHora = fechaHora;
            Lugar = lugar;
            Valor = valor;
        }

        public override string ToString()
        {
            return $"{FechaHora.ToString()}\t{Lugar}\t{Valor}";
        }

        public DateTime FechaHora { get; set; }
        public string Lugar { get; set; }
        public long Valor { get; set; }
    }
}
