﻿using LiveChartsCore.Defaults;
using LiveChartsCore.SkiaSharpView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MedidorDeAfluencia
{
    public partial class MainPage : ContentPage
    {
        int total = 0;
        int entrantes = 0, salientes = 0;
        Timer timer;
        InfluxService service;
        static string token = "_Gz2DPvjEpBN5_P-s2M8hx5DdYaYJqI7JIBrHAt-AxcM24JLstXiFx1VQ3QiHr5qb-eY_w7sZHkfyLDcOaDCSQ==";
        static string host = "http://200.79.179.167:8086";
        static string bucket = "Contador";
        static string org = "ITESHU-ISC";
        //static string token = "kuhv_4y2-TD6ArvHsQa-aDq-fD-QV89A3sJIkstJaFp5do0QXl0pJFc9-_mgypK3Z8ul0-eRMnytjJrun45xiA==";
        //static string host = "https://eastus-1.azure.cloud2.influxdata.com";
        //static string bucket = "Contador";
        //static string org = "espinoza_vfp@hotmail.com";
        public MainPage()
        {
            InitializeComponent();
            service = new InfluxService(host, bucket, org, token);
        }

        private void btnMenos_Clicked(object sender, EventArgs e)
        {
            if (total > 0)
            {
                total--;
                salientes++;
                ActualizaTotal();
            }
        }

        private void ActualizaTotal()
        {
            lblTotal.Text = total.ToString();
        }

        private void btnIniciar_Clicked(object sender, EventArgs e)
        {
            btnMas.IsEnabled = true;
            btnMenos.IsEnabled = true;
            btnIniciar.IsEnabled = false;
            entLugar.IsReadOnly = true;
            timer = new Timer(IngresarDato, null, 0, 30000);
        }

        private void IngresarDato(object state)
        {
            
            service.AlmacenarValor(entLugar.Text, entrantes, salientes, total);
            entrantes = 0;
            salientes = 0;
            ActualizarGrafico();
        }

        private void ActualizarGrafico()
        {
            var datos = service.ObtenerDatosDeLugar(entLugar.Text, "Total");
            List<DateTimePoint> puntos = new List<DateTimePoint>();
            foreach (var item in datos)
            {
                puntos.Add(new DateTimePoint(item.FechaHora, item.Valor));
            }
            var serie = new List<LineSeries<DateTimePoint>>();
            serie.Add(
            new LineSeries<DateTimePoint>
            {
                Values = puntos
            });

            chart.Series = null;
            chart.Series = serie;
        }

        private void btnMas_Clicked(object sender, EventArgs e)
        {
            total++;
            entrantes++;
            ActualizaTotal();
        }
    }
}
