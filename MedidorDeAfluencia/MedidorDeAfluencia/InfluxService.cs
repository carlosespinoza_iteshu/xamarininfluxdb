﻿using InfluxDB.Client.Writes;
using InfluxDB.Client;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MedidorDeAfluencia
{
    public class InfluxService
    {
        string Bucket;
        string Org;
        DateTimeZone systemZone = DateTimeZoneProviders.Tzdb.GetSystemDefault();
        InfluxDBClient client;
        public InfluxService(string host, string bucket, string org, string token)
        {
            Org = org;
            Bucket = bucket;
            

            client = InfluxDBClientFactory.Create(host, token);
        }

        public void AlmacenarValor(string lugar, int ingresos, int salidas, int total)
        {
            using (var writeApi = client.GetWriteApi())
            {
                var punto = PointData.Measurement("Contador")
                    .Tag("Lugar", lugar)
                    .Field("Entrantes", ingresos)
                    .Field("Salientes", salidas)
                    .Field("Total", total)
                    .Timestamp(DateTime.UtcNow, InfluxDB.Client.Api.Domain.WritePrecision.Ms);

                writeApi.WritePoint(punto, Bucket, Org);
            }
        }


        public List<DatoAfluencia> ObtenerDatosDeLugar(string torniquete, string evento)
        {
            return ObtenerDatosDeTorniqueteAsync(torniquete, evento).Result;
        }

        private async Task<List<DatoAfluencia>> ObtenerDatosDeTorniqueteAsync(string lugar, string evento)
        {

            List<DatoAfluencia> datos = new List<DatoAfluencia>();
            string query = $"from(bucket: \"{Bucket}\") |> range(start: -1h) |> filter(fn: (r) => r[\"_measurement\"] == \"Contador\") |> filter(fn: (r) => r[\"Lugar\"] == \"{lugar}\") |> filter(fn: (r) => r[\"_field\"] == \"{evento}\")";

            var result = await client.GetQueryApi().QueryAsync(query, Org);
            result.ForEach(tabla =>
            {
                var data = tabla.Records;
                data.ForEach(dato =>
                {
                    datos.Add(new DatoAfluencia(dato.GetTimeInDateTime().Value, dato.GetField(), (long)dato.GetValue()));
                });
            });
            return datos;
        }
    }
}
