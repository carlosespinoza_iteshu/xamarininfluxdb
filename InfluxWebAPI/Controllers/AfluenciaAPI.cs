﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service;

namespace InfluxWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AfluenciaAPI : ControllerBase
    {
        string token = "_Gz2DPvjEpBN5_P-s2M8hx5DdYaYJqI7JIBrHAt-AxcM24JLstXiFx1VQ3QiHr5qb-eY_w7sZHkfyLDcOaDCSQ==";
        string host = "http://200.79.179.167:8086";
        string bucket = "Contador";
        string org = "ITESHU-ISC";
        InfluxService service;
        public AfluenciaAPI()
        {
            service = new InfluxService(host, bucket, org, token);
        }

        [HttpGet()]
        public ActionResult<List<DatoAfluencia>> GET(string ubicacion, string elemento)
        {
            var r = service.ObtenerDatosDeLugar(ubicacion, elemento);
            return r != null ? Ok(r) : BadRequest();
        }

        [HttpPost()]
        public ActionResult<bool> POST(string lugar, int ingresos, int salidas, int total)
        {
            try
            {
                service.AlmacenarValor(lugar, ingresos, salidas, total);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
