﻿using InfluxDB.Client;
using InfluxDB.Client.Writes;
using Service;

namespace SimuladorDeDatos
{
    internal class Program
    {
        static string token = "_Gz2DPvjEpBN5_P-s2M8hx5DdYaYJqI7JIBrHAt-AxcM24JLstXiFx1VQ3QiHr5qb-eY_w7sZHkfyLDcOaDCSQ==";
        static string host = "http://200.79.179.167:8086";
        static string bucket = "Contador";
        static string org = "ITESHU-ISC";
        static Random random;
        static int totalPersonas;
        static string[] torniquete = { "P1", "P2", "P3", "P4", "E1", "E2" };
        static InfluxService service;
        static void Main(string[] args)
        {
            service = new InfluxService(host,bucket,org,token);
            Console.WriteLine("Simulador de afluencia de personas");
            totalPersonas = 0;
            random = new Random(DateTime.Now.Second);
            Timer timer = new Timer(Lectura, null, 100,100);
            Console.WriteLine("Simulando datos... Presiona una tecla para terminar...");
            Console.ReadLine();
            timer.Dispose();
            foreach (var item in service.ObtenerDatosDeLugar("P1","Total"))
            {
                Console.WriteLine(item);
            }
            foreach (var item in service.ObtenerDatosDeLugar("E1", "Entrantes"))
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Proceso terminado");
            Console.ReadLine();
        }

        private static void Lectura(object? state)
        {
            int numTorniquete = random.Next(0, 5);
            string lugar = torniquete[numTorniquete];
            int personasEntrantes = random.Next(1, 10);
            totalPersonas += personasEntrantes;
            int personasSalientes = random.Next(1, totalPersonas);
            totalPersonas-= personasSalientes;
            service.AlmacenarValor(lugar, personasEntrantes, personasSalientes, totalPersonas);
            Console.WriteLine($"Torniquete: {lugar}\t Entrantes: {personasEntrantes} \t Salientes: {personasSalientes} \t Total: {totalPersonas}");
        }
    }
}